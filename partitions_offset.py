from kafka import SimpleClient
from kafka.protocol.offset import OffsetRequest, OffsetResetStrategy
from kafka.common import OffsetRequestPayload
brokers = 'localhost:29092'
client = SimpleClient(brokers)
topic = 'test3'
partitions = client.topic_partitions[topic]
offset_requests = [OffsetRequestPayload(topic, p, -1, 1) for p in partitions.keys()]

offsets_responses = client.send_offset_request(offset_requests)
partition_map = {}
for r in offsets_responses:
    partition_map.update({r.partition: r.offsets[0]})
    print "partition = %s, offset = %s"%(r.partition, r.offsets[0])

print partition_map
