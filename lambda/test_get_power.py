import yaml

function = yaml.safe_load(open('user_functions.yml'))
t = []
for key, value in function['get_power'].items():
    t.append('lambda '+ str(value))

steps = list(reversed(t))
steps_func = []
result = []
for i in range(len(steps)):
    steps_func.append(eval(steps[i]))
n = 10
for i in range(len(steps)):
    n = (steps_func[i](n,n))
    print n
