import argparse
import sys

class MyParser(argparse.ArgumentParser):
    def error(self, message):
        sys.stderr.write('error: %s\n' % message)
        self.print_help()
        sys.exit(2)
global ip
parser=MyParser()
parser.add_argument('--ip', action="store_true",default=False, dest='ip',help="BROKER ADDRESS")
args=parser.parse_args()
ip = args.ip
if ip is None:
    parser.error("No Address Provided")
print args
if ip:
    print "True"
