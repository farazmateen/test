import argparse
import sys

parser=argparse.ArgumentParser()
parser.add_argument('ip', nargs='+', help="Kafka Broker Address")
if len(sys.argv)==1:
    parser.print_help()
    sys.exit(2)
args=parser.parse_args()
print args.ip[0]
