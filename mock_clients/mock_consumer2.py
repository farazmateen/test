import confluent_kafka
import time
from confluent_kafka import Consumer, Producer, KafkaError

consumer = Consumer({'bootstrap.servers': 'localhost:29092', 'group.id': 'xyz'})
consumer.subscribe(['t2'])
mp = {}
final = {}
while True:
    message = consumer.poll(0)
    if message is None:
        continue
    elif message.value():
        if mp.has_key(message.partition()):
            if message.key() not in mp[message.partition()]:
                mp[message.partition()].append(message.key())
        else:
           for key in mp.keys():
               mp[key] = []
           mp.update({message.partition(): [message.key()]})
        with open('map.log', 'a') as f:
             f.write(str(mp) + '\n')
        for key in mp.keys():
            final.update({key: len(mp[key])})
        print final
