import confluent_kafka
import time
import datetime
from confluent_kafka import Consumer, Producer, KafkaError

#producer = Producer({'bootstrap.servers': 'localhost:29092', 'queue.buffering.max.messages':10000000})
producer = Producer({'bootstrap.servers': 'localhost:29092'})
count = 0
start = time.time()
while True:
    for i in range(1001, 10000):
        producer.produce(topic='t2',key=str(i), value=str(count)) 
        producer.poll(0)
        count += 1 
        time.sleep(0.01)
        print count
producer.flush()
