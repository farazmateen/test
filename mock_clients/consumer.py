import confluent_kafka
import time
from confluent_kafka import Consumer, Producer, KafkaError


def my_assign (consumer, partitions):
    for p in partitions:
        p.offset = confluent_kafka.OFFSET_END
    print('assign', partitions)
    consumer.assign(partitions)

consumer = Consumer({'bootstrap.servers': 'localhost:29092', 'group.id': 'xy1z'})
#consumer.subscribe(['Transformed-Data'], on_assign=my_assign)
print confluent_kafka.OFFSET_END
consumer.subscribe(['Transformed-Data'])
partition = confluent_kafka.TopicPartition('Transformed-Data')
consumer.assign([partition])
while True:
    message = consumer.poll(3)
    if message is None:
        continue
    else:
        print message.value()
        consumer.unassign()
        print partition.offset
        partition.offset -= 1
        time.sleep(2)
        consumer.assign([partition])
