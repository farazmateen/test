#!/bin/bash

apt-get update
apt-get install openjdk-8-* -y
apt-get install curl python-dev gcc python-pip -y
apt-get install redis-server -y
pip install redis
wget http://www-eu.apache.org/dist/kafka/0.10.1.0/kafka_2.10-0.10.1.0.tgz

tar -zxvf kafka_2.10-0.10.1.0.tgz

mv kafka_2.10-0.10.1.0 kafka

cd ~/.

curl -L https://github.com/edenhill/librdkafka/archive/v0.9.2-RC1.tar.gz | tar xzf -
cd librdkafka-0.9.2-RC1/

./configure --prefix=/usr

make -j

sudo make install

cd ~/.

pip install confluent-kafka
pip install kafka-python
pip install numpy
pip install pandas
cd ~/kafka

pip install sympy

pip install pyyaml

pip install IPy
pip install mock
